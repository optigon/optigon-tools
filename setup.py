from setuptools import setup, find_packages

setup(
    name='optigon_tools',
    version='0.1.6',    
    description='Public tools for interfacing with Optigon sofware and hardware',
    url='https://gitlab.com/optigon/optigon-tools',
    author='Brandon Motes',
    author_email='bmotes@optigon.us',
    license='MIT',
    # packages=['dataframetools'],
    packages=find_packages(include=['optigon']),
    install_requires=['matplotlib',
                      'numpy',
                      'scipy',
                      'nptyping',                  
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 3.10',
    ],
)