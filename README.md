# Optigon Tool

This module provides functinoality to interact with Optigon tools, particularly DataFrames. DataFrames are data files containing measurement data and metadata for a measurement point.
## Installation

This package can be installed using the standard python package manager, pip, by running the following command:

`pip install git+https://gitlab.com/optigon/optigon-tools.git`

## Examples

### Generating a mock DataFrame

First import the functions from the module with

`from optigon.dataframe import *`

`from optigon.mockdata.generation import generate_dataframe`

To generate a dataframe, use the generate_dataframe function. This takes a list of measurement names, e.g., ["trpl_1", "pl_1"]. The measurement types are inferred from the names. Each name must contain one of "trpl", "pl", or "trans."

`df = generate_dataframe(["pl", "trpl", "trans"])`

Each dataframe holds a series of measurements (specified by the passed list). The list_meas() function will list all of the stored measurements.

`print(list_meas(df))`

To retrieve a particular measurement's data as a numpy array, pass the dataframe and the measurment name to the extract_numpy() function. Additionally, the keyword argument `normalize` can be used to normalize the returned data. This karg will be ignored when retrieving absorbtivity data.

`pl_numpy_data = extract_numpy(df, "pl", normalize=True)`

The extract_csv() will pull both the data and meta data for a particular measurement and output it to a csv file. The name will be written on the first line followed by the meta data, then the measuremet data. The normalize karg can also be used here.

`extract_csv(df, "pl", "pl_data.csv", normalize=True)`

If you want to quickly view data in the DataFrame, quickview() will generate a matplotlib plot array of the available data. The title keyword argument can be used to set the super title, and norm_trpl and norm_pl can be used to specify normalization of the respective data.

`quickview(df)`


## License

MIT License