from __future__ import annotations

from typing import Union, Optional, List, Tuple, Dict, Any
from nptyping import NDArray, Shape
import json
from dataclasses import dataclass
import numpy as np

@dataclass
class DataFrame():

    VERSION = 2

    # meta data keys
    MK_FORMAT = "0"
    MK_LOCATION = "1"
    MK_SIZE = "2"
    MK_META = "3"
    MK_TYPE = "4"

    # Data Formats
    FORMAT_XY_UINT8 = 0
    FORMAT_XY_UINT16 = 1
    FORMAT_XY_UINT32 = 2
    FORMAT_XY_UINT64 = 3
    
    FORMAT_XY_INT8 = 10
    FORMAT_XY_INT16 = 11
    FORMAT_XY_INT32 = 12
    FORMAT_XY_INT64 = 13

    FORMAT_XY_FLOAT16 = 20
    FORMAT_XY_FLOAT32 = 21
    FORMAT_XY_FLOAT64 = 22

    FORMAT_Y_UINT8 = 70
    FORMAT_Y_UINT16 = 71
    FORMAT_Y_UINT32 = 72
    FORMAT_Y_UINT64 = 73

    FORMAT_Y_INT8 = 80
    FORMAT_Y_INT16 = 81
    FORMAT_Y_INT32 = 82
    FORMAT_Y_INT64 = 83

    FORMAT_Y_FLOAT16 = 90
    FORMAT_Y_FLOAT32 = 91
    FORMAT_Y_FLOAT64 = 92

    # >70 enumeration must map back to n-70 for correct datatype
    dtype_dict = {
        FORMAT_XY_UINT8: np.uint8,
        FORMAT_XY_UINT16: np.uint16,
        FORMAT_XY_UINT32: np.uint32,
        FORMAT_XY_UINT64: np.uint64,

        FORMAT_XY_INT8: np.int8,
        FORMAT_XY_INT16: np.int16,
        FORMAT_XY_INT32: np.int32,
        FORMAT_XY_INT64: np.int64,
        
        FORMAT_XY_FLOAT16: np.float16,
        FORMAT_XY_FLOAT32: np.float32,
        FORMAT_XY_FLOAT64: np.float64,
    }

    def __init__(
        self, 
        meas_name: Union[List[DataFrame], bytes, str],
        meas_type: str = "",
        meta: Optional[Dict] = {},
        data: NDArray = np.empty(0),
        data_format: Optional[int] = None,
    ):
        
        # where an iterable of df has been passed
        if isinstance(meas_name, (list, tuple)):
            self.meta = {}
            self.data_byt = b''
            for df in meas_name:
                df_meta = df.meta.copy()
                df_data = df.data_byt
                cur_data_len = len(self.data_byt)
                for meas_name in df_meta.keys():
                    df_meta[meas_name] = df_meta[meas_name].copy()
                    df_meta[meas_name][DataFrame.MK_LOCATION] += cur_data_len
                    self.data_byt += df_data
                    self.meta.update(df_meta)

        # wehre the raw bytes of a df has been passed
        elif isinstance(meas_name, bytes):
            self._from_bytes(meas_name)

        # where a new df is to be created from arguments
        elif isinstance(meas_name, str):
            self.data_byt = self._gen_data_bytes(data_format, data, meta)
            meta["type"] = meas_type
            spec_meta = {
                DataFrame.MK_FORMAT: data_format,
                DataFrame.MK_META: meta,
                DataFrame.MK_LOCATION: 0,
                DataFrame.MK_SIZE: len(self.data_byt)
            }
            spec_meta.update(meta)
            # spec_meta[self.MK_TYPE] = meas_type
            self.meta = {meas_name: spec_meta}
        else:
            raise ValueError("Unrecognized args")
            
    def _gen_data_bytes(self, form: int, data: np.array, settings: dict) -> bytes:
        try:
            if form >= 70:
                data_type = self.dtype_dict[form-70]
            else:
                data_type = self.dtype_dict[form]
        except KeyError:
            raise ValueError(f"Unrecognized data fromat: {form}.")

        if form < 70:
            return np.concatenate((data[0].astype(data_type), data[1].astype(data_type))).tobytes()
        else:
            assert "bin_width" in settings.keys(), "Data format with y only requires bin_width to be stored in settings."
            return data[1].astype(data_type).tobytes()

    def _data_from_bytes(self, form: int, byts: bytes, settings: Dict) -> np.array:
        try:
            if form >= 70:
                data_type = self.dtype_dict[form-70]
            else:
                data_type = self.dtype_dict[form]
        except KeyError:
            raise ValueError(f"Unrecognized data fromat: {form}.")

        if form <70:
            data = np.frombuffer(byts, dtype=data_type)
            return np.reshape(data, (2, -1)).copy()
        else:
            y = np.frombuffer(byts, dtype=data_type)
            x = np.arange(0, y.size * settings["bin_width"], settings["bin_width"])
            return (np.vstack((x, y)))

    def to_bytes(self) -> bytes:
        version_byt = int(self.VERSION).to_bytes(2, signed=False, byteorder="big")
        meta_byt = json.dumps(self.meta).encode("utf-8")
        meta_len_byt = len(meta_byt).to_bytes(2, signed=False, byteorder="big")
        total_len_byt = int(len(meta_byt) + len(self.data_byt) + 2).to_bytes(4, signed=False, byteorder="big")
        return version_byt + total_len_byt + meta_len_byt + meta_byt + self.data_byt

    def _from_bytes(self, byts) -> None:
        version = int.from_bytes(byts[:2], byteorder="big", signed=False)

        if version != self.VERSION: print("WARNING: Loading outdated DataFrame version. Suggest converting using convert_df_version method.")

        if version == 1 or version == 2 or version == 2:
            total_length = int.from_bytes(byts[2:6], byteorder="big", signed=False)
            meta_len = int.from_bytes(byts[6:8], byteorder="big", signed=False)
            self.meta = json.loads(byts[8:8 + meta_len].decode("utf-8"))
            self.data_byt = byts[8 + meta_len:]
            self.version = version
        else:
            raise RuntimeError(f"Unrecognized DataFrame version: '{version}'.")

    def get_meas_types(self) -> Dict[str: str]:
        """
        Returns dictionary of measurement name and type pairs.
        """
        return dict([(m_name, self.meta[m_name][self.MK_META]["type"]) for m_name in self.meta.keys()])

    def get_data(self, meas_name: str) -> NDArray:
        meta = self.meta[meas_name]
        data_size = meta[DataFrame.MK_SIZE]
        data_loc = meta[DataFrame.MK_LOCATION]
        data_format = meta[DataFrame.MK_FORMAT]
        settings = meta[DataFrame.MK_META]
        return self._data_from_bytes(data_format, self.data_byt[data_loc:data_loc + data_size], settings)
    
    def sub_df(self, meas_name: str) -> NDArray:
        return DataFrame(meas_name, self.get_meas_types()[meas_name], self.get_meta(meas_name), self.get_data(meas_name), self.meta[meas_name][DataFrame.MK_FORMAT])

    def get_data_by_type(self, data_type: str) -> List[NDArray[Shape["2, N"], Any]]:
        data = []
        for m_name in self.meta.keys():
            if self.meta[m_name][self.MK_META]["type"] == data_type:
                data.append(self.get_data(m_name))
        return data

    def get_meta(self, meas_name: str) -> Dict[str, Union[str, int, float]]:
        return self.meta[meas_name][DataFrame.MK_META]

    get_settings = get_meta

    def save(self, filename: str) -> None:
        with open(filename, "wb") as f:
            f.write(self.to_bytes())
            f.close()

    def from_file(filename: str) -> DataFrame:
        with open(filename, "rb") as f:
            byts = f.read()
            f.close()
            return DataFrame(byts)

@dataclass
class _Legacy_DataFrame():

    INT16 = 0
    INT32 = 1
    INT64 = 2
    FLOAT32 = 3
    FLOAT64 = 4
    UINT16 = 5

    # meta data keys
    MK_TYPE = "0"
    MK_START = "1"
    MK_STOP = "2"
    MK_SETTINGS = "3"
    MK_LOCATION = "4"
    MK_SIZE = "5"

    def __init__(
        self, 
        meas: Union[list, bytes, str],
        start_time: Optional[int] = 0,
        stop_time: Optional[int] = 0,
        settings: Optional[List[Union[int, float, str]]] = [],
        data: Optional[np.array] = np.empty(0),
        match_time: Optional[bool] = True,
    ):
        if isinstance(meas, (list, tuple)):
            self.meta = {}
            self.data_byt = b''
            for df in meas:
                df_meta = df.meta.copy()
                df_data = df.data_byt
                cur_data_len = len(self.data_byt)
                for meas_type in df_meta.keys():
                    df_meta[meas_type] = df_meta[meas_type].copy()
                    df_meta[meas_type][DataFrame.MK_LOCATION] += cur_data_len
                    self.data_byt += df_data
                    self.meta.update(df_meta)
        elif isinstance(meas, bytes):
            self._from_bytes(meas)
        elif isinstance(meas, str):
            self.data_byt = data.tobytes()
            spec_meta = {
                DataFrame.MK_TYPE: self._from_dtype(data.dtype),
                DataFrame.MK_START: start_time,
                DataFrame.MK_STOP: stop_time,
                DataFrame.MK_META: settings,
                DataFrame.MK_LOCATION: 0,
                DataFrame.MK_SIZE: len(self.data_byt)
            }
            self.meta = {meas: spec_meta}
        else:
            raise ValueError("Unrecognized args")
            
    def _get_dtype(self, type) -> np.dtype:
        if type == DataFrame.INT16:
            return np.int16
        elif type == DataFrame.INT32:
            return np.int32
        elif type == DataFrame.INT64:
            return np.int64
        elif type == DataFrame.FLOAT32:
            return np.float32
        elif type == DataFrame.FLOAT64:
            return np.float64
        elif type == DataFrame.UINT16:
            return np.uint16
        else:
            raise ValueError("Unrecognized type.")

    def _from_dtype(self, type) -> int:
        if type == np.int16:
            return DataFrame.INT16
        elif type == np.int32:
            return DataFrame.INT32
        elif type == np.int64:
            return DataFrame.INT64
        elif type == np.float32:
            return DataFrame.FLOAT32
        elif type == np.float64:
            return DataFrame.FLOAT64
        elif type == np.uint16:
            return DataFrame.UINT16
        else:
            raise ValueError(f"Unrecognized data type {type}")

    def to_bytes(self) -> bytes:
        meta_byt = json.dumps(self.meta).encode("utf-8")
        meta_len_byt = len(meta_byt).to_bytes(8, signed=False, byteorder="big")
        data_len_byt = len(self.data_byt).to_bytes(8, signed=False, byteorder="big")
        total_len_byt = int(len(meta_byt) + len(self.data_byt) + 16).to_bytes(8, signed=False, byteorder="big")
        return total_len_byt + meta_len_byt + meta_byt + data_len_byt + self.data_byt
        # return meta_len_byt + meta_byt + data_len_byt + self.data_byt

    def _from_bytes(self, byts) -> None:
        meta_len = int.from_bytes(byts[:8], byteorder="big", signed=False)
        self.meta = json.loads(byts[8:8 + meta_len].decode("utf-8"))
        # for type, meta in self.meta.items():
        #     new_dict = {}
        #     for key, val in meta.items():
        #         new_dict[int(key)] = val
        #     self.meta[type] = new_dict
        data_len = int.from_bytes(byts[8 + meta_len:16 + meta_len], signed=True, byteorder="big")
        assert 16 + meta_len + data_len == len(byts), f"Incorrect number of bytes. Delta: {len(byts) - (16 + meta_len + data_len)}"
        self.data_byt = byts[-data_len:]

    def get_meas_types(self) -> Tuple[str]:
        return tuple(self.meta.keys())

    def get_data(self, meas_name: str) -> NDArray:
        spec_meta = self.meta[meas_name]
        data_size = spec_meta[DataFrame.MK_SIZE]
        data_loc = spec_meta[DataFrame.MK_LOCATION]
        type = self._get_dtype(spec_meta[DataFrame.MK_TYPE])
        return np.frombuffer(self.data_byt[data_loc:data_loc + data_size], dtype=type)

    def get_setting_vals(self, meas_name: str) -> List[Union[int, float, str]]:
        return self.meta[meas_name][DataFrame.MK_META]

    def get_times(self, meas_name: str) -> Tuple[int, int]:
        return (self.meta[meas_name][DataFrame.MK_START], self.meta[meas_name][DataFrame.MK_STOP])

    @property
    def is_valid(self) -> bool:
        return all([ms[DataFrame.MK_START] != 0 for ms in self.meta.values()])

    def save(self, filename: str) -> None:
        with open(filename, "wb") as f:
            f.write(self.to_bytes())
            f.close()

    def from_file(filename: str) -> object:
        with open(filename, "rb") as f:
            byts = f.read()
            f.close()
            return DataFrame(byts[8:])


def convert_df_version(df: DataFrame, target_version: int, verbose: bool = False) -> DataFrame:

    if verbose: print("Converting...")
    if verbose: print(f"D\tetected version: {df.version}")

    if df.version == 1 and target_version == 2:
        if verbose: print("\tAdding type metadata.")
        for meas in df.meta.keys():
            if verbose: print(f"\tFound {meas}")
            if "trans" in meas:
                if verbose: print(f"\tSetting type to \'trans\'")
                df.meta[meas][df.MK_META]["type"] = "trans"
            elif "trpl" in meas:
                df.meta[meas][df.MK_META]["type"] = "trpl"
                if verbose: print(f"\tSetting type to \'trpl\'")
            elif "srpl" in meas:
                df.meta[meas][df.MK_META]["type"] = "srpl"
                if verbose: print(f"\tSetting type to \'srpl\'")
            else:
                raise RuntimeError(f"Failed to convert. Could not identify {meas} type.")
        return df
    else:
        raise RuntimeError("Unkown conversion strategy for {df.version} -> {target_version}.")