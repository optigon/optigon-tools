from optigon.core.dataframe import DataFrame
import time
import csv
from typing import List
import numpy as np
import matplotlib.pyplot as plt


def extract_csv(df: DataFrame, meas: str, fname: str, normalize=False) -> None:
    assert meas in df.get_meas_types(), "Cannot find measurement in data frame."
    with open(fname, "w", newline="") as csv_file:
        out = csv.writer(csv_file)
        data = extract_numpy(df, meas, normalize)
        settings = extract_meta(df, meas)
        out.writerow((f"{meas.upper()} Measurement Data",))
        for sett in settings:
            out.writerow((sett, settings[sett]))
        # t = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(df.get_times(meas)[0]))
        # out.writerow(("Collection Time", t))

        if "trpl" in meas:
            if normalize:
                out.writerow(("Time", "Normalized Intensity"))
            else:
                out.writerow(("Time", "Counts"))
        elif "trans" in meas:
            out.writerow(("Wavelength", "Absorption"))
        elif "pl" in meas:
            if normalize:
                out.writerow(("Wavelength", "Normalized Intensity"))
            else:    
                out.writerow(("Wavelength", "Counts"))
        else:
            raise ValueError("Unrecognized measurement type. Cannot generate CSV file.")

        for point in data.T:
            out.writerow(point)
        

def extract_numpy(df: DataFrame, meas: str, normalize=False) -> np.array:
    assert meas in df.get_meas_types(), "Cannot find measurement in data frame."
    data = df.get_data(meas)
    if normalize and not ("trans" in meas):
        return normalize_data(data)
    return data

def extract_meta(df: DataFrame, meas: str) -> dict:
    return df.get_settings(meas)

def normalize_data(data: np.array) -> np.array:
    data[1] /= np.max(data[1])
    return data

def list_meas(df: DataFrame) -> List[str]:
    return df.get_meas_types()

def load_dataframe(fname: str) -> DataFrame:
    return DataFrame.from_file(fname)

def quickview(df: DataFrame, title="DataFrame Quickview", norm_trpl: bool = True, norm_pl: bool = True):

    plt.figure()
    plt.suptitle(title)

    for name, typ in df.get_meas_types().items():
        if typ == "trpl":
            plt.subplot(2,2,2)
            settings = df.get_settings(name)
            data = df.get_data(name)
            data = df.get_data(name).astype(np.float64)
            if norm_trpl:
                data[1] /= np.max(data[1])
            data[0] /= 1000
            plt.semilogy(data[0], data[1], label=name)
            plt.xlim(0, 5000)
            plt.title("Time Resolved Photoluminescence")
            plt.ylabel("Normalized Counts")
            plt.xlabel("Time (ns)")
        elif  typ == "pl" or typ == "srpl":
            plt.subplot(2,2,3)
            settings = df.get_settings(name)
            data = df.get_data(name)
            if norm_pl:
                data[1] = data[1] / np.max(data[1])
            plt.plot(data[0], data[1], label=name)
            plt.title("Photoluminescence Spectrum")
            plt.ylabel("Normalized Intensity")
            plt.xlabel("Wavelength (nm)")
            plt.ylim((0,1.2))
            plt.legend()
        elif  typ == "trans":
            plt.subplot(2,2,1)
            settings = df.get_settings(name)
            data = df.get_data(name)
            plt.plot(data[0], data[1], label=name)
            plt.xlim(450,950)
            plt.ylim((0,1.2))
            plt.title("Transmission")
            plt.ylabel("Transmisson")
            plt.xlabel("Wavelength (nm)")

    plt.show()

def mult_quickview(dfs: Dict[str, DataFrame], title="DataFrame Quickview", norm_trpl: bool = True, norm_pl: bool = True): 

    raise NotImplementedError

    # plt.figure()
    # plt.suptitle(title)
    # plt.tight_layout()

    # for name, df in dfs.items():

    #     for mt in df.get_meas_types():
    #         if mt.split("_")[0] == "trpl":
    #             plt.subplot(2,2,2)
    #             settings = df.get_settings(mt)
    #             data = df.get_data(mt)
    #             trimmed = np.trim_zeros(data[1], "b")
    #             x = data[0, :trimmed.size]
    #             max = np.argmax(trimmed)
    #             data = np.vstack((x, trimmed))[:,max:]
    #             data = data.astype(np.float64)
    #             if norm_trpl:
    #                 data[1] /= np.max(data[1])
    #             data[0] /= 1000
    #             plt.semilogy(data[0], data[1], label=name)
    #             plt.title("Time Resolved Photoluminescence")
    #             plt.ylabel("Normalized Counts")
    #             plt.xlabel("Time (ns)")
    #             plt.legend()
    #         elif  mt.split("_")[0] == "srpl":
    #             plt.subplot(2,2,3)
    #             settings = df.get_settings(mt)
    #             data = df.get_data(mt)
    #             if norm_pl:
    #                 data[1] = data[1] / np.max(data[1])
    #             plt.plot(data[0], data[1], label=name)
    #             plt.title("Photoluminescence Spectrum")
    #             plt.ylabel("Normalized Intensity")
    #             plt.xlabel("Wavelength (nm)")
    #             plt.ylim((0,1.2))
    #             plt.legend()
    #         elif  mt.split("_")[0] == "trans":
    #             plt.subplot(2,2,1)
    #             settings = df.get_settings(mt)
    #             data = df.get_data(mt)
    #             plt.plot(data[0], data[1], label=name)
    #             plt.ylim((0,1.2))
    #             plt.title("Transmission")
    #             plt.ylabel("Transmisson")
    #             plt.xlabel("Wavelength (nm)")
    #             plt.legend()
    #         elif  mt.split("_")[0] == "pl":
    #             print("Detected depracted measurement type: PL. Displaying as SRPL.")
    #             plt.subplot(2,2,3)
    #             settings = df.get_settings(mt)
    #             data = df.get_data(mt)
    #             if norm_pl:
    #                 data[1] = data[1] / np.max(data[1])
    #             plt.plot(data[0], data[1], label=name)
    #             plt.title("Photoluminescence Spectrum")
    #             plt.ylabel("Normalized Intensity")
    #             plt.xlabel("Wavelength (nm)")
    #             plt.ylim((0,1.2))
    #             plt.legend()

    # plt.show()