from optigon.core import DataFrame
import numpy as np
from typing import Dict, Literal, Optional, List, Union
from dataclasses import dataclass, astuple
from optigon.mockdata.utils import *
from time import time_ns

# def generate_dataframe(content: List[str], start_time: int = None, end_time: int = None) -> DataFrame:
def generate_dataframe(content: List[str], params: dict={}) -> DataFrame:
    """
    Takes list of element names and generates a dataframe with the appropriate data. Names must contain data types (pl, trans, trpl).
    Ex: ["pl", "trans", "trpl"] or ["pl_1", "pl_2"]
    The measurement parameters can be specified using the params kwarg. Ex: {"pl": PLParams(...)}
    """

    frames = []
    for meas in content:
        if "trpl" in meas:
            pars = params.get("trpl", TRPLParams())
            settings = {"bin_width": 80 * 2**pars.binning}
            data = generate_trpl(pars)
            form = DataFrame.FORMAT_Y_INT16
        elif "pl" in meas:
            pars = params.get("pl", PLParams())
            settings = {}
            data = generate_pl(pars)
            form = DataFrame.FORMAT_XY_FLOAT32
        elif "trans" in meas:
            pars = params.get("trans", TransParams())
            settings = {}
            data = generate_trans(pars)
            form = DataFrame.FORMAT_XY_FLOAT32
        else:
            raise ValueError("Unrecognized measurement type. Valid name must include one of 'pl', 'trans', 'trpl'")

        frames.append(DataFrame(meas, settings, data, form))

    return DataFrame(frames)

    

## PL Spectrum ##
@dataclass
class PLParams:
    # simulation parameters
    bandgap: float = 720
    fwhm: float = 120
    peak_intensity: float = 200,
    bandgap_dev: int = 30
    seed: Optional[int] = None

def generate_pl(parameters: PLParams) -> np.array:
    bandgap, fwhm, peak_intensity, bandgap_dev, seed = astuple(parameters)

    np.random.seed(seed)

    adj_bandgap = np.random.normal(loc=bandgap, scale=bandgap_dev)

    x = np.linspace(350, 1000, num=3648)

    x, y = gen_led_emission_spectra(x, adj_bandgap, fwhm=fwhm, peak_intensity=peak_intensity)

    y = add_noise(y, noise_std_dev=5, seed=seed,)

    y = np.rint(y).astype(np.int32)
    y = np.maximum(np.zeros(y.size), y)

    return np.vstack((x,y))


## Transmission ##
@dataclass
class TransParams:
    # simulation parameters
    bandgap: int = 800
    bandgap_dev: int = 30
    seed: Optional[int] = None

def generate_trans(parameters: TransParams) -> np.array:
    bandgap, bandgap_dev, seed = astuple(parameters)

    x = np.linspace(350, 1000, num=3648)

    x_abs, abs_spectra = gen_perov_abs_spectra(x, bandgap=bandgap, bandgap_dev=bandgap_dev, seed=seed)

    x_source, source_spectra = gen_bb_light_source_spectra(x)

    thru = (1 - abs_spectra / 100) * source_spectra
 
    noisy_thru = add_noise(thru, noise_std_dev=5, seed=seed)

    y = noisy_thru / (source_spectra + .00001)

    y = np.clip(y, 0, 1)
    y = np.maximum(np.zeros(y.size), y)

    return np.vstack((x,y))


## Time Resolved PL ##
@dataclass
class TRPLParams:
    # measurement parameters
    binning: int = 5 # binning number (base 80ps)
    num_bins: int = 32768
    # simulation parameters
    delay: int = 4000 # ps
    rise: int = 500 # ps
    peak: int = 200 # peak number of counts
    k_nr: int = 1e5 # non-radiative rate (1/cm^3/s)
    noise_std_dev: int = 5
    seed: Optional[int] = None

def generate_trpl(parameters: TRPLParams, interpolating_method: InterpolatingMethod = Spline()) -> np.array:
    binning, num_bins, delay, rise,  peak, k_nr, noise_std_dev, seed = astuple(parameters)

    bin_res = 80 * 2**binning

    x = np.linspace(0, bin_res * num_bins, endpoint=False, num=num_bins)
    x = np.rint(x).astype(np.uint32)

    def _so_decay(t: Union[int, np.array], peak: int, k_nr: int, delay: int):
        """single order decay"""
        if isinstance(t, np.ndarray):
            t = t.astype(np.float64)
        return peak * np.exp((-t + delay) * 1e-12 * k_nr)


    y = _so_decay(x, peak, k_nr, delay)

    y[:int(delay / bin_res)] = 0

    # simple gaussian noise
    y = add_noise(y, noise_std_dev=noise_std_dev)
    y = np.clip(y, a_min=0, a_max = 32000)
    y = np.rint(y).astype(np.uint16)
    
    return np.vstack((x,y))
