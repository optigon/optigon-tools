"""
optigon.

Python library for interacting with Optigon hardware and software.
"""

__version__ = "0.1.0"
__author__ = 'Brandon Motes'
__credits__ = 'Optigon Inc'