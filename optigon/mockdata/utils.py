import numpy as np

from scipy.interpolate import interp1d
from typing import Protocol


## Interpolation Methods
class InterpolatingMethod(Protocol):

	def interpolate(self, x_guide: np.array, y_guide: np.array, x: np.array):
		pass


class Poly(InterpolatingMethod):

	def __init__(self, poly_deg: int = 2):
		self.poly_deg = poly_deg

	def interpolate(self, x_guide: np.array, y_guide: np.array, x: np.array):
		coeffs = np.polyfit(x_guide, y_guide, self.poly_deg)
		y = np.polyval(coeffs, x)
		return y


class Spline(InterpolatingMethod):

	def __init__(self, kind: str = 'cubic'):
		self.kind = kind

	def interpolate(self, x_guide: np.array, y_guide: np.array, x: np.array):
		spline = interp1d(x_guide, y_guide, kind=self.kind)
		y = spline(x)
		return y


def add_noise(inputs: np.array, noise_std_dev: float = 1, seed: int = None) -> np.array:
	"""
	Add gaussian noise to data.

	:param inputs:
	:param noise_std_dev: standard deviation of noise
	:param seed:
	:return:
	"""
	np.random.seed(seed)

	return np.random.normal(loc=inputs, scale=noise_std_dev, size=inputs.size)


def gen_data_from_guides(
	x_guide: np.array,
	y_guide: np.array,
	x: np.array,
	interpolating_method: InterpolatingMethod,
	noise_std_dev: float = 1,
	seed: int = None
) -> np.array:
	"""
	Generate continuous data from interpolation of guide points. Two methods can be
	chosen, polynomial fit or a spline fit.

	:param x_guide: array of x component of guide points
	:param y_guide: array of y component of guide points
	:param x: array of x scale
	:param interpolating_method
	:param noise_std_dev: standard deviation of noise
	:param seed: seed for random generator
	:return:
	"""

	np.random.seed(seed)

	y = interpolating_method.interpolate(x_guide, y_guide, x)
	y = add_noise(y, noise_std_dev=noise_std_dev)

	return x, y


def gen_bb_light_source_spectra(
    x: np.array, 
    peak_intensity: float = 1, 
    seed: int = None, 
    interpolating_method: InterpolatingMethod = Spline()
) -> np.array:
    """
    Generate a spectra for a broadband light sources.

    :param x: wavelengths in nanometers
    :param peak_intensity: max intensity lm/cm^-2
    :param seed: seed for random generator
    :param interpolating_method:
    """

    x_guides = np.array([x[0], 355, 356, 385, 386, 400, 410, 420, 425, 450, 475, 525, 575, 620, 700, 750, 800, 850, 950, x[-1]])
    y_guides = np.array([0, 0, 0, 0, 0, 5, 10, 65, 62, 94, 70, 98, 90, 100, 37, 50, 60, 35, 10, 10])  # relative intensity

    _, y = gen_data_from_guides(x_guides, y_guides, x, interpolating_method, noise_std_dev=.1, seed=seed)

    # Ensure all intensities are >=0
    y = np.maximum(y, np.zeros(y.size))

    # Normalize to specified peak intensity
    y /= peak_intensity / np.max(y)

    return x, y


def gen_led_emission_spectra(x: np.array, bandgap: int, fwhm: int = 80, peak_intensity: int = 80) -> np.array:
	"""
	Generate the emission spectra of an LED, parameterized by the full width half
	max and the bandgap, both in nanometers.
	"""

	std_dev = fwhm / 2.35
	y = 1 / (std_dev*np.sqrt(2*np.pi)) * np.exp(-(x-bandgap)**2 / (2 * std_dev**2))
	y *= peak_intensity / np.max(y)

	return x, y


def gen_perov_abs_spectra(x: np.array, bandgap: int = 800, bandgap_dev: int = 30, seed: int = None) -> np.array:
	"""
	:param x:
	:param bandgap:
	:param bandgap_dev: # bandgap in nm (corresponding photon energy)
	:param seed:
	:return:
	"""

	np.random.seed(seed)

	adj_bandgap = np.random.normal(loc=bandgap, scale=bandgap_dev)

	x_guide = np.array(
		[x[0], adj_bandgap*.4, adj_bandgap*.8, adj_bandgap*.9, adj_bandgap, adj_bandgap*1.1, adj_bandgap*1.2, x[-1]]
	)
	y_guide = np.array([90, 85, 85, 85, 50, 10, 10, 5])

	x, y = gen_data_from_guides(x_guide, y_guide, x, Spline(), noise_std_dev=.1, seed=seed)

	y = np.maximum(y, np.zeros(y.size))
	y = np.minimum(y, np.full(y.size, 100))

	return x, y
